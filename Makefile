GOPATH:=$(shell go env GOPATH)

AppVersion=$(shell git describe --abbrev=0 --tags)
GoVersion=$(shell go env GOVERSION)
OS=$(shell go env GOOS)
Arch=$(shell go env GOARCH)
BuildTime=$(shell date +%FT%T%z)
GitRevision=$(shell git rev-parse --short HEAD)
Branch=$(shell git rev-parse --abbrev-ref HEAD)
LDFLAGS=-ldflags "-X 'gitlab.com/micro-lab/tech-lab/micro-plugins/version.GoVersion=${GoVersion}' \
-X gitlab.com/micro-lab/tech-lab/micro-plugins/version.Version=${AppVersion} \
-X gitlab.com/micro-lab/tech-lab/micro-plugins/version.BuildTime=${BuildTime} \
-X gitlab.com/micro-lab/tech-lab/micro-plugins/version.GitBranch=${Branch} \
-X gitlab.com/micro-lab/tech-lab/micro-plugins/version.OSArch=${OS}/${Arch} \
-X gitlab.com/micro-lab/tech-lab/micro-plugins/version.GitRevision=${GitRevision}"  \

.PHONY: build
build: lint
	go build -v -o user-srv cmd/main.go


.PHONY: release
release:
	go build ${LDFLAGS} -v -o user-srv cmd/main.go


.PHONY: test
test:
	go test -v ./... -cover


.PHONY: proto
proto:
	@chmod -R 755 pkg/proto/build/scripts/*
	@pkg/proto/build/scripts/proto_compile.sh


# Code lint
.PHONY: lint
lint: fmt
	@docker pull diycoder/all-in-one:v1.0
	@docker run --rm  \
	  --env PROJECT_TYPE="make-lint" \
	  -v $(shell pwd):/opt/app -w /opt/app \
	  diycoder/all-in-one:v1.0


# Code format, go import format
.PHONY: fmt
fmt:
	@echo "gofmt -l -s -w ..."
	@ret=0 && for d in $$(go list -f '{{.Dir}}' ./... | grep -v /vendor/); do \
		gofmt -l -s -w $$d/*.go || ret=$$? ; \
		goimports -w $$d/*.go || ret=$$? ; \
	done ; exit $$ret


rebase:
	export branch=`git branch | grep \* | grep -Eo ' .+'` && \
		git checkout master && \
		git pull --rebase && \
		git checkout $$branch && \
		git rebase master;


# 多环境支持，合并开发分支代码到t[1,2,3]，遇到冲突请手动解决冲突
t%:
	@echo "当前对应环境名称: env0$* 分支：t$*";
	- git branch -D t$*;
	git fetch;
	export branch=`git branch | grep \* | grep -Eo ' .+'` && \
		echo "当前分支: $$branch" && \
		git checkout t$* && \
		git pull --rebase && \
		git merge origin/master && \
		echo "merge: \033[0;31morigin/master\033[0m" && \
		git merge $$branch && \
		echo "merge: \033[0;31m$$branch\033[0m" && \
		git push && \
		git checkout $$branch;

rebase:
	export branch=`git branch | grep \* | grep -Eo ' .+'` && \
		git checkout master && \
		git pull --rebase && \
		git checkout $$branch && \
		git rebase master;


# 放弃本地修改
drop:
	git add .; git stash; git stash drop
