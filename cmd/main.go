package main

import (
	"fmt"

	"gitlab.com/coserplay/user-srv/srv/constant"
	"gitlab.com/coserplay/user-srv/srv/handler"
	"gitlab.com/micro-lab/go-micro"
	"gitlab.com/micro-lab/tech-lab/micro-plugins/config/loader/storage"
	_ "gitlab.com/micro-lab/tech-lab/micro-plugins/config/watcher/mysql"
	_ "gitlab.com/micro-lab/tech-lab/micro-plugins/config/watcher/redis"
	"gitlab.com/micro-lab/tech-lab/micro-plugins/plugin"
	"gitlab.com/micro-proto/user-srv/user"
)

func main() {
	Init()
	// New Service
	service := micro.NewService()

	// Initialize user-srv
	service.Init()

	if err := user.RegisterUserHandler(
		service.Server(), handler.NewUserHandler(),
	); err != nil {
		panic(fmt.Errorf("registry user handler panic: %v ", err))
	}

	// run user-srv
	if err := service.Run(); err != nil {
		panic(fmt.Errorf("service run panic: %v ", err))
	}
}

func Init() {
	// server name
	if err := storage.SetServerName(constant.UserService); err != nil {
		panic(fmt.Errorf("set server name panic: %v ", err))
	}

	// register address
	if err := storage.SetRegisterAddr("http://127.0.0.1:2379"); err != nil {
		panic(fmt.Errorf("set reg addr panic: %v ", err))
	}

	// apollo config
	config := storage.NewStorage(
		storage.WithAddress("http://118.25.3.190:8080"),
		storage.WithAppID("user-srv"),
		storage.WithCluster("dev"),
		storage.WithNamespace("application"),
		storage.WithBackupPath("./config"),
		storage.WithBackup(true),
	)

	// storage apollo config
	if err := config.Store(); err != nil {
		panic(fmt.Errorf("store apollo config panic: %v ", err))
	}

	// init plugin
	if err := plugin.InitPlugins(plugin.DefaultPlugins()...); err != nil {
		panic(fmt.Errorf("init plugins panic: %v ", err))
	}
}
