# user-srv

## 运行 Etcd

```
docker run -d \
  -p 2379:2379 \
  -p 2380:2380 \
  --mount type=bind,source=/tmp/etcd-data.tmp,destination=/etcd-data \
  --name etcd-gcr-v3.4.14 \
  gcr.io/etcd-development/etcd:v3.4.14 \
  /usr/local/bin/etcd \
  --name s1 \
  --data-dir /etcd-data \
  --listen-client-urls http://0.0.0.0:2379 \
  --advertise-client-urls http://0.0.0.0:2379 \
  --listen-peer-urls http://0.0.0.0:2380 \
  --initial-advertise-peer-urls http://0.0.0.0:2380 \
  --initial-cluster s1=http://0.0.0.0:2380 \
  --initial-cluster-token tkn \
  --initial-cluster-state new \
  --log-level info \
  --logger zap \
  --log-outputs stderr
```

## 运行 user-srv
```go
go run cmd/main.go --registry=etcd
```

## 运行网关
```go
micro --registry=etcd --auth_namespace=coserplay --server_name=coserplay.api.gateway --api_address=0.0.0.0:8088 api --namespace=coserplay.api --handler=http --enable_rpc
```

