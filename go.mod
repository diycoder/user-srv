module gitlab.com/coserplay/user-srv

go 1.16

require (
	github.com/go-redis/redis/v8 v8.11.4
	github.com/jmoiron/sqlx v1.3.4
	gitlab.com/micro-err/user-srv v1.0.0
	gitlab.com/micro-lab/go-micro v0.0.1
	gitlab.com/micro-lab/tech-lab v0.0.2-0.20220108161441-23467cf7a368
	gitlab.com/micro-proto/user-srv v1.0.0
)

replace (
	gitlab.com/micro-err/user-srv => ./pkg/errcode
	gitlab.com/micro-proto/user-srv => ./pkg/proto
)
