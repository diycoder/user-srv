FROM golang:1.16-alpine3.14 as build

ENV GO111MODULE=on
ENV GOPROXY=https://goproxy.cn,direct
RUN apk add --no-cache --update \
    bash \
    git \
    mercurial \
    make \
    openssh-client \
    gcc \
    libc-dev \
    tzdata

WORKDIR /data/src
COPY . /data/src
RUN GOOS=linux CGO_ENABLED=0 GOARCH=amd64 go build -ldflags="-s -w" -installsuffix cgo -o user-srv cmd/main.go

# 运行：使用scratch作为基础镜像
FROM alpine:3.14 as prod

# 在build阶段复制时区到
COPY --from=build /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
# 在build阶段复制可执行的go二进制文件app
COPY --from=build /data/src/user-srv /data/bin/

WORKDIR /data