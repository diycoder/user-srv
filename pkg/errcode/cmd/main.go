package main

import (
	"gitlab.com/micro-lab/tech-lab/kit/error-gen/cmd"
)

/**
生成的代码格式
package  errcode

const (
	StatusOK	=	200// RFC 7231, 6.3.1
	StatusCreated	=	201// RFC 7231, 6.3.2
	StatusInternalServerError	=	500// RFC 7231, 6.6.1
)

var errorMsg = map[int]string{
	StatusInternalServerError	:	"Internal Server Error",
	StatusOK	:	"ok",
	StatusCreated	:	"Created",
}

func ErrorMsg(code int) string {
	return errorMsg[code]
}

*/

func main() {
	cmd.Run()
}
