package errcode

/*
*错误码
 */
const (
	Success       = 0
	Error         = 500
	MissingData   = 400001
	DataStatus    = 400002
	ParamIllegal  = 400003
	UserNotExists = 400004
)

var errorMsg = map[int]string{
	Success:       "操作成功",
	Error:         "系统异常",
	MissingData:   "数据缺失",
	DataStatus:    "数据参数不正确，请勿非法操作",
	ParamIllegal:  "参数传入不合法:[%s]",
	UserNotExists: "用户不存在",
}

func ErrorMsg(code int) string {
	return errorMsg[code]
}
