module gitlab.com/micro-proto/user-srv

go 1.16

require (
	github.com/gogo/protobuf v1.3.2
	github.com/golang/protobuf v1.5.2
	gitlab.com/micro-lab/go-micro v0.0.1
	golang.org/x/mod v0.5.0 // indirect
	golang.org/x/sys v0.0.0-20211210111614-af8b64212486 // indirect
	golang.org/x/text v0.3.7 // indirect
)
