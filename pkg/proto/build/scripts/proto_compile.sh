#!/usr/bin/env bash

set -o errexit
set -o nounset
set -o pipefail

PROTO_ROOT="$(cd "$(dirname "${BASH_SOURCE}")/../.." && pwd -P)"
DOC_TEMPLATE="doc-md-zh"
DIR=$(basename "${PROTO_ROOT}")

PROJECT_TYPE="make-proto"
PROTO_PATH="$(go list -m)"

docker pull diycoder/all-in-one:v1.0

docker run \
  -t --rm  \
  -v ${PROTO_ROOT}:/go/src/$PROTO_PATH \
  --env PROJECT_TYPE=$PROJECT_TYPE \
  --env DOC_TEMPLATE=$DOC_TEMPLATE \
  --env PROTO_PATH=${PROTO_PATH} \
  diycoder/all-in-one:v1.0

