
<a name="top"></a>



<a name="user/user.proto"></a>
<p align="right"></p>

## user/user.proto 接口文档




<a name="user.User"></a>

### User


| 方法名 | 请求类型 | 响应类型 | 描述 |
| ----------- | ------------ | ------------- | ------------|
| GetUser | [GetUserReq](#user.GetUserReq) | [GetUserResp](#user.GetUserResp) | 获取用户信息 |
| BatchGetUser | [BatchGetUserReq](#user.BatchGetUserReq) | [BatchGetUserResp](#user.BatchGetUserResp) | 批量获取用户信息 |
| GetUserList | [GetUserListReq](#user.GetUserListReq) | [GetUserListResp](#user.GetUserListResp) | 获取用户列表 |
| UpdateUser | [UpdateUserReq](#user.UpdateUserReq) | [UpdateUserResp](#user.UpdateUserResp) | 更新用户 |

 <!-- end services -->



<a name="user.BatchGetUserReq"></a>

### BatchGetUserReq



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user_id | [int64](#int64) | repeated | 用户ID列表 |






<a name="user.BatchGetUserResp"></a>

### BatchGetUserResp



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user_list | [UserInfo](#user.UserInfo) | repeated | 用户信息列表 |






<a name="user.GetUserListReq"></a>

### GetUserListReq



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user_id | [int64](#int64) |  | 用户ID |
| user_name | [string](#string) |  | 用户名称 |
| page | [int64](#int64) |  | 第几页 |
| count | [int64](#int64) |  | 每页数目 |






<a name="user.GetUserListResp"></a>

### GetUserListResp



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user_list | [UserInfo](#user.UserInfo) | repeated | 用户列表 |
| count | [int64](#int64) |  | 总数目 |






<a name="user.GetUserReq"></a>

### GetUserReq



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user_id | [int64](#int64) |  | 用户ID |
| user_name | [string](#string) |  | 用户名称 |






<a name="user.GetUserResp"></a>

### GetUserResp



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user | [UserInfo](#user.UserInfo) |  |  |






<a name="user.UpdateUserReq"></a>

### UpdateUserReq



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user_id | [int64](#int64) |  | 用户ID |
| param | [string](#string) |  | 用户信息 |






<a name="user.UpdateUserResp"></a>

### UpdateUserResp



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| result | [bool](#bool) |  |  |






<a name="user.UserInfo"></a>

### UserInfo



| 字段 | 类型 | 标签 | 描述 |
| ----- | ---- | ----- | ----------- |
| user_id | [int64](#int64) |  | 用户ID |
| user_name | [string](#string) |  | 用户名称 |
| user_nick | [string](#string) |  | 用户昵称 |
| user_status | [int64](#int64) |  | 用户状态 |
| avatar | [string](#string) |  | 用户头像 |
| cover_image_phone | [string](#string) |  | 用户封面 |
| description | [string](#string) |  | 个人介绍 |
| gender | [int64](#int64) |  | 性别 |
| verified | [int64](#int64) |  | 是否验证 |
| verified_reason | [string](#string) |  | 验证原因 |





 <!-- end messages -->

 <!-- end enums -->

 <!-- end HasExtensions -->




## proto类型与各个语言类型对应关系

| .proto Type | Notes | C++ | Java | Python | Go | C# | PHP | Ruby |
| ----------- | ----- | --- | ---- | ------ | -- | -- | --- | ---- |
| <a name="double" /> double |  | double | double | float | float64 | double | float | Float |
| <a name="float" /> float |  | float | float | float | float32 | float | float | Float |
| <a name="int32" /> int32 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint32 instead. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="int64" /> int64 | Uses variable-length encoding. Inefficient for encoding negative numbers – if your field is likely to have negative values, use sint64 instead. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="uint32" /> uint32 | Uses variable-length encoding. | uint32 | int | int/long | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="uint64" /> uint64 | Uses variable-length encoding. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum or Fixnum (as required) |
| <a name="sint32" /> sint32 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int32s. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sint64" /> sint64 | Uses variable-length encoding. Signed int value. These more efficiently encode negative numbers than regular int64s. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="fixed32" /> fixed32 | Always four bytes. More efficient than uint32 if values are often greater than 2^28. | uint32 | int | int | uint32 | uint | integer | Bignum or Fixnum (as required) |
| <a name="fixed64" /> fixed64 | Always eight bytes. More efficient than uint64 if values are often greater than 2^56. | uint64 | long | int/long | uint64 | ulong | integer/string | Bignum |
| <a name="sfixed32" /> sfixed32 | Always four bytes. | int32 | int | int | int32 | int | integer | Bignum or Fixnum (as required) |
| <a name="sfixed64" /> sfixed64 | Always eight bytes. | int64 | long | int/long | int64 | long | integer/string | Bignum |
| <a name="bool" /> bool |  | bool | boolean | boolean | bool | bool | boolean | TrueClass/FalseClass |
| <a name="string" /> string | A string must always contain UTF-8 encoded or 7-bit ASCII text. | string | String | str/unicode | string | string | string | String (UTF-8) |
| <a name="bytes" /> bytes | May contain any arbitrary sequence of bytes. | string | ByteString | str | []byte | ByteString | string | String (ASCII-8BIT) |
