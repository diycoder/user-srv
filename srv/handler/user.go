package handler

import (
	"context"

	"gitlab.com/coserplay/user-srv/srv/service"
	"gitlab.com/micro-proto/user-srv/user"
)

type UserHandler struct {
	us service.UserSRV
}

func NewUserHandler() *UserHandler {
	return &UserHandler{
		us: service.NewUserService(),
	}
}

func (u *UserHandler) GetUser(ctx context.Context, req *user.GetUserReq, resp *user.GetUserResp) error {
	user, err := u.us.GetUser(ctx, req.UserId, req.UserName)
	if err != nil {
		return err
	}
	resp.User = user
	return nil
}

func (u *UserHandler) BatchGetUser(ctx context.Context, req *user.BatchGetUserReq, resp *user.BatchGetUserResp) error {
	user, err := u.us.BatchGetUser(ctx, req.UserId)
	if err != nil {
		return err
	}
	resp.UserList = user
	return nil
}

func (u *UserHandler) GetUserList(ctx context.Context, req *user.GetUserListReq, resp *user.GetUserListResp) error {
	list, total, err := u.us.GetUserList(ctx, req.UserId, req.UserName, req.Page, req.Count)
	if err != nil {
		return err
	}
	resp.Count = total
	resp.UserList = list
	return nil
}

func (u *UserHandler) UpdateUser(ctx context.Context, req *user.UpdateUserReq, resp *user.UpdateUserResp) error {
	result, err := u.us.UpdateUser(ctx, req.UserId, req.Param)
	if err != nil {
		return err
	}
	resp.Result = result
	return nil
}
