package handler

import (
	"context"
	"fmt"
	"log"
	"os"
	"testing"

	"gitlab.com/micro-lab/tech-lab/micro-plugins/config"
	"gitlab.com/micro-lab/tech-lab/micro-plugins/config/loader/storage"
)

var (
	ctx     context.Context
	handler *UserHandler
)

func TestMain(t *testing.M) {
	// apollo config
	cfg := storage.NewStorage(
		storage.WithAddress("http://118.25.3.190:8080"),
		storage.WithAppID("user-srv"),
		storage.WithCluster("dev"),
		storage.WithNamespace("application"),
		storage.WithBackupPath("./config"),
		storage.WithBackup(true),
	)

	// storage apollo config
	if err := cfg.Store(); err != nil {
		panic(fmt.Errorf("store apollo config panic: %v ", err))
	}

	err := config.Just4Test()
	if err != nil {
		log.Fatal(err)
	}

	handler = NewUserHandler()
	ctx = context.Background()

	t.Run()
	os.Exit(0)
}

func TestNewUserHandler(t *testing.T) {
	user, err := handler.us.GetUser(ctx, 18591633457021032, "")
	t.Log(user, err)
}

func TestGetUserList(t *testing.T) {
	userID, name, page, count := int64(0), "", int64(1), int64(10)
	userList, total, err := handler.us.GetUserList(ctx, userID, name, page, count)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(userList, total)
}

func TestBatchGetUser(t *testing.T) {
	userID := []int64{18591633457021032}
	user, err := handler.us.BatchGetUser(ctx, userID)
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(user)
}
