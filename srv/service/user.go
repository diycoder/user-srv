package service

import (
	"context"
	"encoding/json"
	"fmt"

	"gitlab.com/coserplay/user-srv/srv/model"
	"gitlab.com/coserplay/user-srv/srv/repository"
	"gitlab.com/coserplay/user-srv/srv/tools/common"
	userErr "gitlab.com/micro-err/user-srv"
	"gitlab.com/micro-lab/tech-lab/micro-plugins/log"
	"gitlab.com/micro-proto/user-srv/user"
)

type UserSRV interface {
	GetUser(ctx context.Context, userID int64, userName string) (*user.UserInfo, error)
	BatchGetUser(ctx context.Context, userIds []int64) ([]*user.UserInfo, error)
	GetUserList(ctx context.Context, userID int64, name string, page, count int64) ([]*user.UserInfo, int64, error)
	UpdateUser(ctx context.Context, userID int64, param string) (bool, error)
}

type UserService struct {
	userRepo repository.UserRepository
}

func NewUserService() UserSRV {
	return &UserService{
		userRepo: repository.NewUserRepo(),
	}
}

// GetUser 用户信息查询
func (u *UserService) GetUser(ctx context.Context, userID int64, userName string) (*user.UserInfo, error) {
	if userID <= 0 && userName == "" {
		return nil, common.ErrorNew(userErr.ParamIllegal)
	}

	// todo 优先读缓存

	// 读DB
	us, err := u.userRepo.GetUser(ctx, userID, userName)
	if err != nil {
		log.Errorf("GetUser user_id:%v, err:%v", userID, err)
		return nil, err
	}
	if us == nil {
		log.Errorf("user user_id:%v not exist", userID)
		return nil, common.ErrorNew(userErr.UserNotExists)
	}
	return &user.UserInfo{
		UserId:          us.UserID,
		Avatar:          us.Avatar,
		Gender:          us.Gender,
		Verified:        us.Verified,
		UserName:        us.UserName,
		UserNick:        us.UserNick,
		UserStatus:      us.UserStatus,
		Description:     us.Description,
		VerifiedReason:  us.VerifiedReason,
		CoverImagePhone: us.CoverImagePhone,
	}, nil
}

// BatchGetUser 批量查询用户信息
func (u *UserService) BatchGetUser(ctx context.Context, userIds []int64) ([]*user.UserInfo, error) {
	if userIds == nil {
		return nil, common.Errorf(userErr.ParamIllegal, fmt.Sprintf("userIds:%v", userIds))
	}
	users, err := u.userRepo.BatchGetUser(ctx, userIds)
	if err != nil {
		log.Errorf("BatchGetUser userIds:%v, err:%v", userIds, err)
		return nil, err
	}
	return u.convertUser(users), nil
}

// GetUserList 用户列表查询
func (u *UserService) GetUserList(ctx context.Context, userID int64, name string, page, count int64) ([]*user.UserInfo, int64, error) {
	if page <= 0 || count <= 0 {
		return nil, 0, common.Errorf(userErr.ParamIllegal, fmt.Sprintf("page:%v, count:%v", page, count))
	}

	list, total, err := u.userRepo.GetUserList(ctx, userID, name, page, count)
	if err != nil {
		log.Errorf("GetUserList user_id:%v, name:%v, page:%v, count:%v, err:%v",
			userID, name, page, count, err)
		return nil, 0, err
	}
	return u.convertUser(list), total, nil
}

func (u *UserService) UpdateUser(ctx context.Context, userID int64, param string) (bool, error) {
	data := make(map[string]interface{})
	err := json.Unmarshal([]byte(param), &data)
	if err != nil {
		log.Errorf("UpdateUser Unmarshal user_id:%v, param:%v, err:%v", userID, param, err)
		return false, common.ErrorNew(userErr.ParamIllegal)
	}
	err = u.userRepo.UpdateUser(ctx, userID, data)
	if err != nil {
		log.Errorf("UpdateUser user_id:%v, param:%v, err:%v", userID, data, err)
		return false, err
	}
	return true, nil
}

// 转换为 rpc response user
func (u *UserService) convertUser(list []*model.User) []*user.UserInfo {
	data := make([]*user.UserInfo, 0)
	if len(list) == 0 {
		return data
	}

	for _, us := range list {
		data = append(data, &user.UserInfo{
			UserId:          us.UserID,
			UserName:        us.UserName,
			UserNick:        us.UserNick,
			UserStatus:      us.UserStatus,
			Avatar:          us.Avatar,
			CoverImagePhone: us.CoverImagePhone,
			Description:     us.Description,
			Gender:          us.Gender,
			Verified:        us.Verified,
			VerifiedReason:  us.VerifiedReason,
		})
	}

	return data
}
