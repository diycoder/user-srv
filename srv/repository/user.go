package repository

import (
	"bytes"
	"context"
	"database/sql"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/coserplay/user-srv/srv/client"
	"gitlab.com/coserplay/user-srv/srv/model"
	"gitlab.com/micro-lab/tech-lab/micro-plugins/log"
)

type UserRepository interface {
	GetUser(context.Context, int64, string) (*model.User, error)
	BatchGetUser(context.Context, []int64) ([]*model.User, error)
	GetUserList(ctx context.Context, userID int64, name string, page, count int64) ([]*model.User, int64, error)
	UpdateUser(ctx context.Context, userID int64, param map[string]interface{}) error
}

type UserRepo struct {
	userDB *sqlx.DB
}

func NewUserRepo() UserRepository {
	return &UserRepo{
		userDB: client.GetDB(),
	}
}

// GetUser 查询用户
func (u *UserRepo) GetUser(ctx context.Context, userID int64, userName string) (*model.User, error) {
	query := ` select user_id, user_name, user_nick, status, avatar, cover_image_phone, 
        description, gender, verified, verified_reason from user where user_id > 0 `
	args := make([]interface{}, 0)
	if userID > 0 {
		query += `and user_id = ? `
		args = append(args, userID)
	}
	if strings.TrimSpace(userName) != "" {
		query += `and user_name = ? `
		args = append(args, userName)
	}
	var user model.User
	err := u.userDB.GetContext(ctx, &user, query, args...)
	if err == sql.ErrNoRows {
		return nil, nil
	}
	if err != nil {
		log.Errorf("GetUser user_id:%v, user_name:%v, err:%v", userID, userName, err)
		return nil, err
	}
	return &user, nil
}

// BatchGetUser 批量查询用户
func (u *UserRepo) BatchGetUser(ctx context.Context, userIds []int64) ([]*model.User, error) {
	query := ` select user_id, user_name, user_nick, status, avatar, cover_image_phone,
        description, gender, verified, verified_reason from user where user_id in (?) `
	query, values, err := sqlx.In(query, userIds)
	if err != nil {
		log.Errorf("BatchGetUser sqlx.In user_ids:%v, err:%v", userIds, err)
		return nil, err
	}
	list := make([]*model.User, 0)
	err = u.userDB.SelectContext(ctx, &list, query, values...)
	if err != nil {
		log.Errorf("BatchGetUser user_ids:%v, err:%v", userIds, err)
		return nil, err
	}
	return list, nil
}

// GetUserList 用户列表查询
func (u *UserRepo) GetUserList(ctx context.Context, userID int64, name string, page, count int64) ([]*model.User, int64, error) {
	query := ` select user_id, user_name, user_nick, status, avatar, cover_image_phone, 
        description, gender, verified, verified_reason from user where user_id > 0 `
	countSQL := `select count(1) as count from user where user_id > 0 `
	order := " order by user_id desc "
	args, countArgs := make([]interface{}, 0), make([]interface{}, 0)

	if userID > 0 {
		query += " and user_id = ? "
		countSQL += " and user_id = ? "
		args = append(args, userID)
		countArgs = append(countArgs, userID)
	}
	if name != "" {
		query += " and name = ? "
		countSQL += " and name = ? "
		args = append(args, name)
		countArgs = append(countArgs, name)
	}
	if page > 0 && count > 0 {
		query += order
		query += " limit ?,? "
		args = append(args, (page-1)*count, count)
	}

	total, err := u.getCount(ctx, countSQL, countArgs)
	if err != nil {
		log.Errorf("GetUserList getCount user_id:%v, name:%v, page:%v, count:%v, err:%v",
			userID, name, page, count, err)
		return nil, 0, err
	}

	list := make([]*model.User, 0)
	err = u.userDB.SelectContext(ctx, &list, query, args...)
	if err != nil {
		log.Errorf("GetUserList Select user_id:%v, name:%v, page:%v, count:%v, err:%v",
			userID, name, page, count, err)
		return nil, 0, err
	}
	return list, total, nil
}

// UpdateUser 用户更新
func (u *UserRepo) UpdateUser(ctx context.Context, userID int64, param map[string]interface{}) error {
	if len(param) == 0 {
		return fmt.Errorf("invalid param: %v", param)
	}
	buffer := bytes.Buffer{}
	args := make([]interface{}, 0)
	for k, v := range param {
		buffer.WriteString(fmt.Sprintf("set %s = ?,", k))
		args = append(args, v)
	}
	s := buffer.String()
	column := s[:len(s)-1]
	query := " update user set %s where user_id = ? "
	args = append(args, userID)
	query = fmt.Sprintf(query, column)
	_, err := u.userDB.ExecContext(ctx, query, args...)
	if err != nil {
		log.Errorf("UpdateUser user_id:%v, param:%v, err:%v", userID, param, err)
		return err
	}
	return nil
}

func (u *UserRepo) getCount(ctx context.Context, query string, args []interface{}) (int64, error) {
	var count int64
	err := u.userDB.GetContext(ctx, &count, query, args...)
	if err != nil {
		log.Errorf("getCount query:%v, args:%v", query, args)
		return 0, err
	}
	return count, nil
}
