package common

import (
	"fmt"

	"gitlab.com/coserplay/user-srv/srv/constant"
	err "gitlab.com/micro-err/user-srv"
	"gitlab.com/micro-lab/go-micro/errors"
)

func ErrorNew(code int) error {
	return errors.New(constant.UserService, err.ErrorMsg(code), int32(code))
}

func Errorf(code int, a ...interface{}) error {
	return errors.New(constant.UserService, fmt.Sprintf(err.ErrorMsg(code), a...), int32(code))
}
