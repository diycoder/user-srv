package client

import (
	"github.com/go-redis/redis/v8"
	"github.com/jmoiron/sqlx"
	"gitlab.com/micro-lab/tech-lab/micro-plugins/config/watcher/mysql"
	rds "gitlab.com/micro-lab/tech-lab/micro-plugins/config/watcher/redis"
)

func GetRedis() *redis.Client {
	client, _ := rds.GetClient("default")
	return client
}

func GetDB() *sqlx.DB {
	db, _ := mysql.GetDB("user")
	return db
}
