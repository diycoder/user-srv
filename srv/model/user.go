package model

type User struct {
	UserID          int64  `db:"user_id"`
	UserName        string `db:"user_name"`
	UserNick        string `db:"user_nick"`
	UserStatus      int64  `db:"status"`
	Avatar          string `db:"avatar"`
	CoverImagePhone string `db:"cover_image_phone"`
	Description     string `db:"description"`
	Gender          int64  `db:"gender"`
	Verified        int64  `db:"verified"`
	VerifiedReason  string `db:"verified_reason"`
}
